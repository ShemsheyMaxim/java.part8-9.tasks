package Task1;

import javax.jws.soap.SOAPBinding;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Максим on 06.04.2017.
 */
public class Weather {

    public static Map<String, Integer> weather = new ConcurrentHashMap<>();
    public static Map<String, String> englishLanguage = new HashMap<>();
    public static Map<String, String> russianLanguage = new HashMap<>();
    public static Map<String, String> ukrainianLanguage = new HashMap<>();

    public static void main(String[] args) {
        Thread threadClass = new MyThread();
        threadClass.start();
        Thread englishClass = new EnglishThread();
        englishClass.start();
        Thread russianClass = new RussianThread();
        russianClass.start();
        Thread ukrainianClass = new UkrainianThread();
        ukrainianClass.start();

        weather.put("temperature", 16);
        weather.put("pressure", 740);
        weather.put("air humidity", 53);

        englishLanguage.put("temperature", "temperature");
        englishLanguage.put("pressure", "pressure");
        englishLanguage.put("air humidity", "air humidity");

        russianLanguage.put("temperature", "температура");
        russianLanguage.put("pressure", "давление");
        russianLanguage.put("air humidity", "влажность воздуха");

        ukrainianLanguage.put("temperature", "температура повітря");
        ukrainianLanguage.put("pressure", "тиск");
        ukrainianLanguage.put("air humidity", "вологість повітря");


        for (Map.Entry result : weather.entrySet()) {
            System.out.println(result.getKey() + " " + result.getValue());
        }
    }

    static class MyThread extends Thread {
        Random random = new Random();

        @Override
        public void run() {
            while (true) {
                try {
                    Thread.sleep(2 * 1000);
                } catch (InterruptedException e) {
                    //DO NOTHING
                }
                weather.put("temperature", random.nextInt(71) - 35);
                weather.put("pressure", 720 + (int) (Math.random() * ((780 - 720) + 1)));
                weather.put("air humidity", 68 + (int) (Math.random() * ((80 - 68) + 1)));
            }
        }
    }

    static class EnglishThread extends Thread {
        int t = 0;
        int p = 0;
        int h = 0;

        @Override
        public void run() {
            while (true) {
                try {
                    Thread.sleep(3 * 1000);
                } catch (InterruptedException e) {
                    //DO NOTHING
                }
                if (t != weather.get("temperature") || p != weather.get("pressure") || h != weather.get("air humidity")) {
                    t = weather.get("temperature");
                    p = weather.get("pressure");
                    h = weather.get("air humidity");
                    System.out.println("English language: " + englishLanguage.get("temperature") + ": " + t + "°C " + englishLanguage.get("pressure") + ": " + p + " мм " + englishLanguage.get("air humidity") + ": " + h + "%");
                }
            }
        }
    }

    static class RussianThread extends Thread {
        int t = 0;
        int p = 0;
        int h = 0;

        @Override
        public void run() {
            while (true) {
                try {
                    Thread.sleep(3 * 1000);
                } catch (InterruptedException e) {
                    //DO NOTHING
                }
                if (t != weather.get("temperature") || p != weather.get("pressure") || h != weather.get("air humidity")) {
                    t = weather.get("temperature");
                    p = weather.get("pressure");
                    h = weather.get("air humidity");
                    System.out.println("Russian language: " + russianLanguage.get("temperature") + ": " + t + "°C " + russianLanguage.get("pressure") + ": " + p + " мм " + russianLanguage.get("air humidity") + ": " + h + "%");
                }
            }
        }
    }

    static class UkrainianThread extends Thread {
        int t = 0;
        int p = 0;
        int h = 0;

        @Override
        public void run() {
            while (true) {
                try {
                    Thread.sleep(3 * 1000);
                } catch (InterruptedException e) {
                    //DO NOTHING
                }
                if (t != weather.get("temperature") || p != weather.get("pressure") || h != weather.get("air humidity")) {
                    t = weather.get("temperature");
                    p = weather.get("pressure");
                    h = weather.get("air humidity");
                    System.out.println("Ukrainian language: " + ukrainianLanguage.get("temperature") + ": " + t + "°C " + ukrainianLanguage.get("pressure") + ": " + p + " мм " + ukrainianLanguage.get("air humidity") + ": " + h + "%");
                }
            }
        }
    }
}
